﻿using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Api.Albums.Responses
{
    public class GetAlbumVideosResponse : ApiResponse
    {
        [JsonProperty("videos")]
        public Model.VideoCollection Videos { get; set; }
    }
}
