﻿using System;
using System.Collections.Generic;

namespace BlackBayou.Vimeo.Api
{
    public class ApiMethod : IApiMethod
    {
        public string Method
        {
            get;
            private set;
        }

        public Dictionary<string, object> Arguments
        {
            get;
            private set;
        }

        public ApiMethod(string method)
        {
            if (string.IsNullOrEmpty(method)) throw new ArgumentNullException("method");

            Method = method;
            Arguments = new Dictionary<string, object>();
        }
    }
}
