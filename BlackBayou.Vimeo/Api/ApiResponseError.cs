﻿using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Api
{
    public class ApiResponseError
    {
        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("msg")]
        public string Message { get; set; }

    }
}
