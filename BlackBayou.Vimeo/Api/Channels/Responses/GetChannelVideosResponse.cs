﻿using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Api.Channels.Responses
{
    public class GetChannelVideosResponse : ApiResponse
    {
        [JsonProperty("videos")]
        public Model.VideoCollection Videos { get; set; }
    }
}
