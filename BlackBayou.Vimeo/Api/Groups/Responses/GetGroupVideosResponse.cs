﻿using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Api.Groups.Responses
{
    public class GetGroupVideosResponse : ApiResponse
    {
        [JsonProperty("videos")]
        public Model.VideoCollection Videos { get; set; }
    }
}
