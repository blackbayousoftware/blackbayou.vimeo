﻿using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Api.Groups.Responses
{
    public class GetGroupsResponse : ApiResponse
    {
        [JsonProperty("groups")]
        public Model.GroupCollection Groups { get; set; }
    }
}
