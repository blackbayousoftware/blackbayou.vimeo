﻿using System.Collections.Generic;

namespace BlackBayou.Vimeo.Api
{
    public interface IApiMethod
    {
        string Method { get; }
        Dictionary<string, object> Arguments { get; }
    }
}
