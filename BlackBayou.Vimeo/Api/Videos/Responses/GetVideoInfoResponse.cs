﻿using System.Collections.Generic;
using BlackBayou.Vimeo.Model;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Api.Videos.Responses
{
    public class GetVideoInfoResponse : ApiResponse
    {
        [JsonProperty("video")]
        public IList<Video> Items { get; set; }
    }
}
