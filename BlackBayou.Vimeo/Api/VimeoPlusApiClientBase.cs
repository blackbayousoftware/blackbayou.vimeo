﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using BlackBayou.Vimeo.OAuth;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth;
using DotNetOpenAuth.OAuth.ChannelElements;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Api
{
    public abstract class VimeoPlusApiClientBase
    {
        public const string VIMEO_PLUS_API_URL_BASE = "http://vimeo.com/api/rest/v2?";

        /// <summary>
        /// Manages the tokens for the OAUTH protocol
        /// </summary>
        private IConsumerTokenManager tokenManager;

        /// <summary>
        /// Access Token to be used for authenticating all requests to vimeo
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tokenManager"></param>
        /// <param name="accessToken"></param>
        public VimeoPlusApiClientBase(IConsumerTokenManager tokenManager, string accessToken)
        {
            this.tokenManager = tokenManager;
            this.AccessToken = accessToken; 
        }

        #region Private Url Builder Methods
        private static string GetApiMethodUrl(string methodName, Dictionary<string, object> methodParameters)
        {
            if (string.IsNullOrEmpty(methodName))
            {
                throw new ArgumentNullException("methodName", "methodName is required.");
            }

            var urlBuilder = new StringBuilder(VIMEO_PLUS_API_URL_BASE);

            // Build the dictionary to contain all of the query string parameters for this request
            var urlParameters = new Dictionary<string, string>
            {
                { "format", "json"},
                { "method", methodName}
            };

            // Add the method's parameters to the list of urlParameters
            if (methodParameters != null)
            {
                methodParameters
                    .ToList()
                    .ForEach(pair => urlParameters.Add(pair.Key, pair.Value == null ? null : pair.Value.ToString()));
            }

            // Append parameter to the url
            var parameters = urlParameters.ToList()
                .Select(pair =>
                    string.IsNullOrEmpty(pair.Value) ?
                        pair.Key :
                        string.Format("{0}={1}", pair.Key, pair.Value))
                .ToArray();

            // Add the parameters to the url string
            urlBuilder.Append(string.Join("&", parameters));

            return urlBuilder.ToString();
        } 
        #endregion

        protected T ExecuteRequest<T>(IApiMethod apiCommand) where T : ApiResponse
        {
            if (apiCommand == null) throw new ArgumentNullException("apiCommand"); 

            // Create the API client
            var vimeo = new WebConsumer(ServiceProviderDescriptionFactory.GetVimeoServiceDescription(), tokenManager);
            
            // Build the url for the api command
            var requestUrl = GetApiMethodUrl(apiCommand.Method, apiCommand.Arguments);

            // Build the endpoint to call the request upon
            var endpoint = new MessageReceivingEndpoint(requestUrl, HttpDeliveryMethods.GetRequest);
            
            // Build and execute the request
            var response = vimeo.PrepareAuthorizedRequestAndSend(endpoint, AccessToken); 

            // Extract the results as JSON
            var resultJson = response.GetResponseReader().ReadToEnd();

            // Parse the results to a domain object to return
            return JsonConvert.DeserializeObject<T>(resultJson);
        }
    }
}
