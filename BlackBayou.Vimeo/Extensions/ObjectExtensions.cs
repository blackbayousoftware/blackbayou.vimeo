﻿using System.ComponentModel;

namespace BlackBayou.Vimeo.Extensions
{
    public static class ObjectExtensions
    {
        public static T? ToNullable<T>(this object input) where T : struct
        {
            var converter = TypeDescriptor.GetConverter(typeof(T?));
            return (T?)converter.ConvertFrom(input);
        }
    }
}
