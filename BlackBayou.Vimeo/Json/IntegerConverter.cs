using System;
using BlackBayou.Vimeo.Extensions;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Json
{
    public class IntegerConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToNullable<int>().GetValueOrDefault(0));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return reader.Value.ToNullable<int>().GetValueOrDefault(0);
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string) || objectType == typeof(int);
        }
    }
}