﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class AlbumCollection : CollectionBase
    {
        [JsonProperty("album")]
        public IList<Album> Items { get; set; }
    }
}
