﻿using System;
using System.Collections.Generic;
using BlackBayou.Vimeo.Json;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class Group
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("has_joined")]
        [JsonConverter(typeof(BooleanConverter))]
        public bool HasJoined { get; set; }

        [JsonProperty("is_featured")]
        [JsonConverter(typeof(BooleanConverter))]
        public bool IsFeatured { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("created_on")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("modified_on")]
        public DateTime ModifiedDate { get; set; }

        [JsonProperty("total_videos")]
        public int TotalVideos { get; set; }

        [JsonProperty("total_members")]
        public int TotalMembers { get; set; }

        [JsonProperty("total_threads")]
        public int TotalThreads { get; set; }

        [JsonProperty("total_files")]
        public int TotalFiles { get; set; }

        [JsonProperty("total_events")]
        public int TotalEvents { get; set; }

        [JsonProperty("url")]
        public IList<string> Urls { get; set; }

        [JsonProperty("thumbnail_url")]
        public string ThumbnailUrl { get; set; }

        [JsonProperty("permissions")]
        public GroupPermissions Permissions { get; set; }

        [JsonProperty("creator")]
        public Person Creator { get; set; }
    }
}
