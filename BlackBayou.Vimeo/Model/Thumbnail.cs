﻿using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class Thumbnail
    {
        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("_content")]
        public string Url { get; set; }
    }
}
