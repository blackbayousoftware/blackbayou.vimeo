﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class ThumbnailCollection
    {
        [JsonProperty("thumbnail")]
        public IList<Thumbnail> Items { get; set; }
    }
}
