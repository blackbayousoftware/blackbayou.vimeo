﻿using System;
using BlackBayou.Vimeo.Json;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class Video
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("embed_privacy")]
        public string EmbedPrivacy { get; set; }

        [JsonProperty("is_hd")]
        [JsonConverter(typeof(BooleanConverter))]
        public bool IsHd { get; set; }

        [JsonProperty("is_watchlater")]
        [JsonConverter(typeof(BooleanConverter))]
        public bool IsWatchLater { get; set; }

        [JsonProperty("license")]
        public string License { get; set; }

        [JsonProperty("modified_date")]
        public DateTime ModifiedDate { get; set; }

        [JsonProperty("privacy")]
        public string Privacy { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("upload_date")]
        public DateTime UploadDate { get; set; }

        [JsonProperty("number_of_likes")]
        [JsonConverter(typeof(IntegerConverter))]
        public int NumberOfLikes { get; set; }

        [JsonProperty("number_of_plays")]
        [JsonConverter(typeof(IntegerConverter))]
        public int NumberOfPlays { get; set; }

        [JsonProperty("number_of_comments")]
        [JsonConverter(typeof(IntegerConverter))]
        public int NumberOfComments { get; set; }

        [JsonProperty("width")]
        [JsonConverter(typeof(IntegerConverter))]
        public int Width { get; set; }

        [JsonProperty("height")]
        [JsonConverter(typeof(IntegerConverter))]
        public int Height { get; set; }

        [JsonProperty("duration")]
        [JsonConverter(typeof(IntegerConverter))]
        public int Duration { get; set; }

        [JsonProperty("owner")]
        public Person Owner { get; set; }

        [JsonProperty("urls")]
        public UrlCollection Urls { get; set; }

        [JsonProperty("thumbnails")]
        public ThumbnailCollection Thumbnails { get; set; }

    }
}
