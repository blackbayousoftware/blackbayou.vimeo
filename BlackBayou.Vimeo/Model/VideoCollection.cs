﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class VideoCollection : CollectionBase
    {
        [JsonProperty("video")]
        public IList<Video> Items { get; set; }
    }
}
