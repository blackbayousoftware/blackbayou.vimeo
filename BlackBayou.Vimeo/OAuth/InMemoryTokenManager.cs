﻿using System;
using System.Collections.Generic;
using DotNetOpenAuth.OAuth.ChannelElements;
using DotNetOpenAuth.OAuth.Messages;

namespace BlackBayou.Vimeo.OAuth
{
    public class InMemoryTokenManager : IConsumerTokenManager
    {
        private Dictionary<string, string> tokensAndSecrets = new Dictionary<string, string>();

        public string ConsumerKey { get; private set; }

        public string ConsumerSecret { get; private set; }

        #region Constructor
        public InMemoryTokenManager(string consumerKey, string consumerSecret, string accessToken, string accessSecret)
            :this(consumerKey, consumerSecret, new Dictionary<string,string>{ { accessToken, accessSecret } })
        {
        }
        public InMemoryTokenManager(string consumerKey, string consumerSecret, Dictionary<string, string> initialTokens = null)
        {
            if (String.IsNullOrEmpty(consumerKey))
            {
                throw new ArgumentNullException("consumerKey");
            }

            this.ConsumerKey = consumerKey;
            this.ConsumerSecret = consumerSecret;

            if (initialTokens != null)
            {
                this.tokensAndSecrets = new Dictionary<string, string>(initialTokens);
            }
        }
        #endregion

        #region ITokenManager Members

        public string GetConsumerSecret(string consumerKey)
        {
            if (consumerKey == this.ConsumerKey)
            {
                return this.ConsumerSecret;
            }
            else
            {
                throw new ArgumentException("Unrecognized consumer key.", "consumerKey");
            }
        }

        public string GetTokenSecret(string token)
        {
            return this.tokensAndSecrets[token];
        }

        public void StoreNewRequestToken(UnauthorizedTokenRequest request, ITokenSecretContainingMessage response)
        {
            this.tokensAndSecrets[response.Token] = response.TokenSecret;
        }

        public void ExpireRequestTokenAndStoreNewAccessToken(string consumerKey, string requestToken, string accessToken, string accessTokenSecret)
        {
            this.tokensAndSecrets.Remove(requestToken);
            this.tokensAndSecrets[accessToken] = accessTokenSecret;
        }

        /// <summary>
        /// Classifies a token as a request token or an access token.
        /// </summary>
        /// <param name="token">The token to classify.</param>
        /// <returns>Request or Access token, or invalid if the token is not recognized.</returns>
        public TokenType GetTokenType(string token)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Public Methods
        public void StoreNewRequestToken(string token, string tokenSecret)
        {
            this.tokensAndSecrets[token] = tokenSecret;
        }
        #endregion Public Methods
    }
}
