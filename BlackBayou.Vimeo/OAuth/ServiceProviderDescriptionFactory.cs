﻿using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth;
using DotNetOpenAuth.OAuth.ChannelElements;

namespace BlackBayou.Vimeo.OAuth
{
    internal class ServiceProviderDescriptionFactory
    { 
        public static class Constants
        {
           public static string ACCESS_TOKEN = "https://vimeo.com/oauth/access_token";
            public static string REQUEST_TOKEN = "https://vimeo.com/oauth/request_token";
            public static string USER_AUTHORIZE = "https://vimeo.com/oauth/authorize";
        }

        public static ServiceProviderDescription GetVimeoServiceDescription()
        {
            return new ServiceProviderDescription
            {
                AccessTokenEndpoint = new MessageReceivingEndpoint(Constants.ACCESS_TOKEN, HttpDeliveryMethods.PostRequest),
                RequestTokenEndpoint = new MessageReceivingEndpoint(Constants.REQUEST_TOKEN, HttpDeliveryMethods.PostRequest),
                UserAuthorizationEndpoint = new MessageReceivingEndpoint(Constants.USER_AUTHORIZE, HttpDeliveryMethods.PostRequest),
                TamperProtectionElements = new ITamperProtectionChannelBindingElement[] { new HmacSha1SigningBindingElement() },
                ProtocolVersion = ProtocolVersion.V10a
            };
        }
    }
}
