﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("BlackBayou.Vimeo.Tests")]
[assembly: AssemblyDescription("Vimeo Advanced API wrapper library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Black Bayou Software, LLC")]
[assembly: AssemblyProduct("BlackBayou.Vimeo.Tests")]
[assembly: AssemblyCopyright("Copyright ©  2013, Black Bayou Software, LLC")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("4eed9a3b-0b76-4c28-a70d-25d955f0073c")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.3.0")]
[assembly: AssemblyFileVersion("1.0.3.0")]
